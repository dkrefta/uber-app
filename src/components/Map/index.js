import React, {Component} from 'react';
import {View, Image} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {getPixelSize} from '../../utils/utils';
import Geolocation from '@react-native-community/geolocation';
import Search from '../Search';
import Geocoder from 'react-native-geocoding';
import Directions from '../Directions';
import {
  LocationBox,
  LocationText,
  LocationTimeBox,
  LocationTimeText,
  LocationTimeTextSmall,
  Back,
} from './styles';
import Details from '../Details';
import markerImage from '../../assets/marker.png';
import backImage from '../../assets/back.png';

Geocoder.init('AIzaSyDvhmXiU0vl-gQvNc1bRmsydO04hj00gNA');
export default class Map extends Component {
  state = {
    region: null,
    destination: null,
    duration: null,
    location: null,
  };
  async componentDidMount() {
    Geolocation.getCurrentPosition(
      async ({coords: {latitude, longitude}}) => {
        const response = await Geocoder.from({latitude, longitude});
        const address = response.results[0].formatted_address;
        const location = address.substring(0, address.indexOf(','));
        this.setState({
          location,
          region: {
            latitude,
            longitude,
            latitudeDelta: 0.0143,
            longitudeDelta: 0.0134,
          },
        });
      }, //success
      () => {}, //error
      {
        timeout: 3000,
        enableHighAccuracy: true,
      },
    );
  }
  handleLocationSelected = (data, {geometry}) => {
    const {
      location: {lat: latitude, lng: longitude},
    } = geometry;
    this.setState({
      destination: {
        latitude,
        longitude,
        title: data.structured_formatting.main_text,
      },
    });
  };

  handleBack = () => {
    this.setState({
      destination: null,
    });
  };

  render() {
    const {region, destination, duration, location} = this.state;
    return (
      <View style={{flex: 1}}>
        <MapView
          style={{flex: 1}}
          region={region}
          showsUserLocation
          loadingEnabled
          ref={el => (this.mapView = el)}>
          {destination && (
            <>
              <Directions
                origin={region}
                destination={destination}
                onReady={result => {
                  this.setState({
                    duration: Math.floor(result.duration),
                  });
                  this.mapView.fitToCoordinates(result.coordinates, {
                    edgePadding: {
                      top: getPixelSize(50),
                      left: getPixelSize(50),
                      right: getPixelSize(50),
                      bottom: getPixelSize(350),
                    },
                  });
                }}
              />
              <Marker
                coordinate={destination}
                anchor={{x: 0, y: 0}}
                image={markerImage}>
                <LocationBox>
                  <LocationText> {destination.title}</LocationText>
                </LocationBox>
              </Marker>
              <Marker
                coordinate={region}
                anchor={{x: 0, y: 0}}
                image={markerImage}>
                <LocationBox>
                  <LocationTimeBox>
                    <LocationTimeText>{duration}</LocationTimeText>
                    <LocationTimeTextSmall>Min</LocationTimeTextSmall>
                  </LocationTimeBox>
                  <LocationText> {location}</LocationText>
                </LocationBox>
              </Marker>
            </>
          )}
        </MapView>
        {destination ? (
          <>
            <Back onPress={this.handleBack}>
              <Image source={backImage} />
            </Back>
            <Details />
          </>
        ) : (
          <Search onLocationSelected={this.handleLocationSelected} />
        )}
      </View>
    );
  }
}

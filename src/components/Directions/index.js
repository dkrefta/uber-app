import React from 'react';
import MapViewDirections from 'react-native-maps-directions';

const Directions = ({destination, origin, onReady}) => {
  return (
    <MapViewDirections
      destination={destination}
      origin={origin}
      onReady={onReady}
      apikey="AIzaSyDvhmXiU0vl-gQvNc1bRmsydO04hj00gNA"
      strokeWidth={3}
      strokeColor="#222"
    />
  );
};

export default Directions;
